package ru.nirinarkhova.tm.api;

public interface ITaskController {

    void showList();

    void create();

    void clear();

    void showTaskByIndex();

    void showTaskByName();

    void showTaskById();

    void removeTaskByIndex();

    void removeTaskByName();

    void removeTaskById();

    void updateTaskByIndex();

    void updateTaskById();

    void startTaskById();

    void startTaskByName();

    void startTaskByIndex();

    void finishTaskById();

    void finishTaskByName();

    void finishTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByName();

    void changeTaskStatusByIndex();

}
