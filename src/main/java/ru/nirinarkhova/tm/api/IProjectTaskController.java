package ru.nirinarkhova.tm.api;

public interface IProjectTaskController {


    void showTaskByProjectId();

    void bindTaskByProject();

    void unbindTaskFromProject();

    void removeProjectById();

}
