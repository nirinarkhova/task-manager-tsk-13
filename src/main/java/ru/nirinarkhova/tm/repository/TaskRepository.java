package ru.nirinarkhova.tm.repository;

import ru.nirinarkhova.tm.api.ITaskRepository;
import ru.nirinarkhova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public void add(final Task task) {
        list.add(task);
    }

    @Override
    public void remove(final Task task) {
        list.remove(task);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task: list) {
            if(id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    public Task findOneByIndex(final Integer index) {
       return list.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task: list) {
            if(name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task findOneByProjectId(String projectId) {
        for (final Task task: list) {
            if(projectId.equals(task.getProjectId())) return task;
        }
        return null;
    }

    @Override
    public Task bindTaskByProject(String taskId, String projectId) {
        if (projectId == null) return null;
        if (taskId == null) return null;
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        final List<Task> projectTasks = new ArrayList<>();
        for (final Task task: list){
            if (projectId.equals(task.getProjectId())) projectTasks.add(task);
        }
        return projectTasks;
    }

    @Override
    public void removeAllByProjectId(String projectId) {
        list.removeIf(task -> projectId.equals(task.getProjectId()));
    }

    @Override
    public Task unbindTaskByProject(String taskId) {
        if (taskId == null) return null;
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}
